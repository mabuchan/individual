json.array!(@workers) do |worker|
  json.extract! worker, :id, :invoice_id, :patient_id, :dermatologist, :diagnostic
  json.url worker_url(worker, format: :json)
end
