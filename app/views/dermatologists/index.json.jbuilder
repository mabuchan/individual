json.array!(@dermatologists) do |dermatologist|
  json.extract! dermatologist, :id, :name, :diagnostic_code
  json.url dermatologist_url(dermatologist, format: :json)
end
