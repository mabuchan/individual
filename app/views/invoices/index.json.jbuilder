json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :number, :name, :amount
  json.url invoice_url(invoice, format: :json)
end
