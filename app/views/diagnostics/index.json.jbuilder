json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :number, :name, :import
  json.url diagnostic_url(diagnostic, format: :json)
end
