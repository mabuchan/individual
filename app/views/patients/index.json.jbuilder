json.array!(@patients) do |patient|
  json.extract! patient, :id, :name, :phone, :reason, :dermatologist, :appointment_date
  json.url patient_url(patient, format: :json)
end
