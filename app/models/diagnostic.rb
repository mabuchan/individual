class Diagnostic < ActiveRecord::Base
  belongs_to :dermatologist
end
def self.import(file)
  CSV.foreach(file.path, headers: true) do |row|
    Diagnostic.create! row.to.hash

  end
end