class Dermatologist < ActiveRecord::Base
  has_many :appointments
  has_many :patients, :through => :appointments
  has_many :invoices, :through => :appointments
end
