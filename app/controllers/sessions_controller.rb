class SessionsController < ApplicationController
  before_action :set_session, only: [:show, :edit, :update, :destroy]

    def create
   session[:password] = params[:password]
      flash[:notice] = 'Successfully Logged in'
      redirect_to user_path
    end
  def destroy
    reset_session
    flash[:notice] = 'Successfully Logged out'
    redirect_to user_path
  end
  end