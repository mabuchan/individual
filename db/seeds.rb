# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Dermatologist.update_all
Appointment.update_all

# Create Dermatologists
dermatologist_hash = {"Dr. Love" => "Romance", "Dr. Revival" => "Rejuvenation", "Dr. Double Dee" => "Augmentation", "Dr. Fine" => "Curation","Dr. Saviour" => "Resuscitation", "Dr. Hero" => "Wonders", "Dr. Triplexxx" => "Pornography", "Dr. Soul" => "Pleasure"}

dermatologists = []
dermatologist_hash.keys.each do |key|
  dermatologists << Dermatologist.create(name: key, speciality: dermatologist_hash[key] )
end


