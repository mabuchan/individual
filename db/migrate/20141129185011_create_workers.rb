class CreateWorkers < ActiveRecord::Migration
  def change
    create_table :workers do |t|
      t.integer :invoice_id
      t.integer :patient_id
      t.integer :dermatologist
      t.integer :diagnostic

      t.timestamps
    end
  end
end
