class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :Patient
      t.integer :Dermatologist
      t.integer :Office_Worker
      t.integer :Admin

      t.timestamps
    end
  end
end
