class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :number
      t.integer :patient_id
      t.integer :dermatologist_id
      t.integer :diagnostic_code_id
      t.decimal :amount

      t.timestamps
    end
  end
end
