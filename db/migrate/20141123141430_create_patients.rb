class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :name
      t.text :phone
      t.text :reason
      t.integer :dermatologist
      t.datetime :appointment_date

      t.timestamps
    end
  end
end
