class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.text :number
      t.text :name
      t.string :import

      t.timestamps
    end
  end
end
