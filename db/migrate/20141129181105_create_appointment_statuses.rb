class CreateAppointmentStatuses < ActiveRecord::Migration
  def change
    create_table :appointment_statuses do |t|
      t.integer :appointment_status_id
      t.text :name

      t.timestamps
    end
  end
end
