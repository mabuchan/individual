class CreateDermatologists < ActiveRecord::Migration
  def change
    create_table :dermatologists do |t|
      t.string :name
      t.integer :diagnostic_code

      t.timestamps
    end
  end
end
